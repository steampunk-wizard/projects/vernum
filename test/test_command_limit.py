from unittest import TestCase

from wizlib.input_handler import InputHandler
from wizlib.config_handler import ConfigHandler

from vernum.command.limit_command import LimitCommand
from vernum.error import VerNumError


class TestLimitCommand(TestCase):

    def test_limit(self):
        tx = "v1.2.3"
        c = LimitCommand(input=InputHandler.fake(tx),
                         config=ConfigHandler.fake(vernum_limit_min='v1.1.0'))
        c.execute()
        self.assertEqual(c.status, 'v1.2.3 >= v1.1.0')

    def test_limit_fail(self):
        tx = "v1.2.3"
        c = LimitCommand(input=InputHandler.fake(tx),
                         config=ConfigHandler.fake(vernum_limit_min='v1.3.0'))
        with self.assertRaises(VerNumError):
            c.execute()

    def test_limit_invalid(self):
        tx = "v1.a"
        c = LimitCommand(input=InputHandler.fake(tx),
                         config=ConfigHandler.fake(vernum_limit_min='v1.3.0'))
        with self.assertRaises(VerNumError):
            c.execute()
