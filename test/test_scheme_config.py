from unittest import TestCase

from wizlib.input_handler import InputHandler
from wizlib.config_handler import ConfigHandler
from vernum.error import VerNumError

from vernum.command.max_command import MaxCommand
from vernum.command.next_command import NextCommand


class TestSchemeConfig(TestCase):

    def test_max_patch_scheme(self):
        tx = "v1.2.3\nv1.0.6"
        input = InputHandler.fake(tx)
        config = ConfigHandler.fake(vernum_scheme='patch')
        c = MaxCommand(input=input, config=config)
        n = c.execute()
        self.assertEqual(n, '1.2.3')

    def test_max_minor_scheme(self):
        tx = "v1.2\nv1.0"
        input = InputHandler.fake(tx)
        config = ConfigHandler.fake(vernum_scheme='minor')
        c = MaxCommand(input=input, config=config)
        n = c.execute()
        self.assertEqual(n, '1.2')

    def test_next_minor_scheme(self):
        input = InputHandler.fake("4.6")
        config = ConfigHandler.fake(vernum_scheme='minor')
        c = NextCommand(input=input, config=config, increment='minor')
        n = c.execute()
        self.assertEqual(n, '4.7')

    def test_invalid_scheme(self):
        input = InputHandler.fake("4.6")
        config = ConfigHandler.fake(vernum_scheme='blah')
        c = MaxCommand(input=input, config=config)
        with self.assertRaises(VerNumError):
            n = c.execute()
