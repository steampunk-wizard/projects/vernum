from unittest import TestCase
from unittest.mock import Mock
from unittest.mock import patch
from io import StringIO
from tempfile import NamedTemporaryFile

from wizlib.input_handler import InputHandler
from test import testfile

from vernum.command.next_command import NextCommand
from vernum.command import VerNumCommand
from vernum import VerNumApp
from vernum.error import VerNumError


class TestCommandNext(TestCase):

    def test_next_nothing(self):
        c = NextCommand(input=InputHandler.fake("v1.2.3"))
        with self.assertRaises(VerNumError):
            c.execute()

    def test_major(self):
        c = NextCommand(input=InputHandler.fake("v1.2.3"), increment='major')
        n = c.execute()
        self.assertEqual(n, '2.0.0')

    def test_from_app(self):
        with patch('sys.stdout', o := StringIO()):
            with patch('sys.stderr', e := StringIO()):
                with testfile('v1.2.3') as file:
                    VerNumApp.run('-i', file.name, 'next', 'patch', debug=True)
        o.seek(0)
        self.assertEqual(o.read(), '1.2.4')
        e.seek(0)
        self.assertEqual(e.read(),
                         'Version number incremented from 1.2.3 to 1.2.4\n')

    def test_invalid_increment(self):
        c = NextCommand(input=InputHandler.fake("v1.2.3"), increment='alpha')
        with self.assertRaises(VerNumError):
            c.execute()

    def test_invalid_input_string(self):
        c = NextCommand(input=InputHandler.fake("v1.g.3"), increment='patch')
        with self.assertRaises(VerNumError):
            c.execute()

    # def test_minor_from_handler(self):
    #     h = CommandHandler(VerNumCommand)
    #     with testfile("v1.2.3") as file:
    #         r, s = h.handle(["-i", file.name, 'next', "minor"])
    #         self.assertEqual(r, '1.3.0')
    #         self.assertEqual(s, 'Version number
    #               incremented from 1.2.3 to 1.3.0')

    # def test_from_stdin(self):
    #     with patch('sys.stdin', StringIO("v1.2.3\n1.3.0\nv1.2.9")):
    #         r, s = CommandHandler(VerNumCommand).handle(['max'])
    #         self.assertEqual(r, '1.3.0')
