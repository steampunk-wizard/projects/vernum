from tempfile import NamedTemporaryFile


def testfile(input):
    file = NamedTemporaryFile('w')
    file.write(input)
    file.seek(0)
    return file
